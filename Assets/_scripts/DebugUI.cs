﻿using UnityEngine;
using UnityEngine.UI;

public class DebugUI : MonoBehaviour
{
    public Text debug;

    public ShipManager boidManager;
    private int ticksPerFpsUpdate = 10;
    private int ticksLeft;

    private void Update()
    {
        if (!boidManager)
            return;
                
        ticksLeft--;
        if (ticksLeft <= 0)
        {
            debug.text = "Alive ships: " + boidManager.aliveBoidsCount + "\nFPS: " + (int)(1.0f / Time.smoothDeltaTime);
            ticksLeft = ticksPerFpsUpdate;
        }
    }
}
