﻿using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    public float speed;

	void Update ()
    {
        transform.LookAt(Vector3.zero);
        transform.Translate(Vector3.right * Time.deltaTime * speed);
    }
}
