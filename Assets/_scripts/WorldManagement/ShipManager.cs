﻿/*
 *  Piotr Bobin, 2017, interview project
 *  Manages CPU heavy ship update methods. Additionaly decide if spawn of next ship is possible, based on stronger side advantage.
 *  
 *  Updates methods have 2 different update rates.
 *      - shipMainUpdateRate - updating target velocity, shooting and target. Too high value will result in more collisions between ships.
 *      - shipVelocityUpdateRate - updating rigidbody.velocity. Too high value will result in jerky movement.
 */

using UnityEngine;

public class ShipManager : MonoBehaviour
{
    private BoostedList<Ship> aliveShipsA;
    private BoostedList<Ship> aliveShipsB;
        
    // ManualUpdateShip
    [Range(1, 10)]
    public int shipMainUpdateRate;
    private int mainStartIndex = 0;

    // UpdateCurrentVelocity
    [Range(1, 5)]
    public int shipVelocityUpdateRate;
    private int velocityStartIndex = 0;

    // Describe maximum advantage ratio of stronger side. Prevent from overwhelming advantage.
    [Range(1f, 2f)]
    public float maxAdvantageRatio;

    public int aliveBoidsCount { get { return aliveShipsA.Count + aliveShipsB.Count; } }

    private void Awake()
    {
        aliveShipsA = new BoostedList<Ship>();
        aliveShipsB = new BoostedList<Ship>();
    }

    private void Update()
    {
        // ManualUpdateShip
        for (int i = mainStartIndex; i < aliveShipsA.Count; i += shipMainUpdateRate)
            aliveShipsA[i].ManualUpdateShip();
        for (int i = mainStartIndex; i < aliveShipsB.Count; i += shipMainUpdateRate)
            aliveShipsB[i].ManualUpdateShip();
        mainStartIndex = (mainStartIndex + 1) % shipMainUpdateRate;

        // UpdateCurrentVelocity
        for (int i = velocityStartIndex; i < aliveShipsA.Count; i += shipVelocityUpdateRate)
            aliveShipsA[i].UpdateCurrentVelocity(shipVelocityUpdateRate);
        for (int i = velocityStartIndex; i < aliveShipsB.Count; i += shipVelocityUpdateRate)
            aliveShipsB[i].UpdateCurrentVelocity(shipVelocityUpdateRate);
        velocityStartIndex = (velocityStartIndex + 1) % shipVelocityUpdateRate;
    }

    public void AddSpawnedShip(Ship ship, SIDE boidSide)
    {
        if (boidSide == SIDE.a)
            aliveShipsA.Add(ship);
        else
            aliveShipsB.Add(ship);

        ship.ManualUpdateShip();
        ship.UpdateCurrentVelocity(shipVelocityUpdateRate);
    }

    public void RemoveDestroyedShip(Ship ship, SIDE boidSide)
    {
        if (boidSide == SIDE.a)
            aliveShipsA.Remove(ship);
        else
            aliveShipsB.Remove(ship);
    }
    
    public bool IsSpawnPossible(SIDE boidSide)
    {
        if ((boidSide == SIDE.a && aliveShipsA.Count <= aliveShipsB.Count * maxAdvantageRatio)
            || (boidSide == SIDE.b && aliveShipsB.Count <= aliveShipsA.Count * maxAdvantageRatio))
            return true;
        else
            return false;
    }
}
