﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Spawning flocks and ships(boids).
 */

using UnityEngine;

[System.Serializable]
public struct SpawnerConfig
{
    public Vector3 position;
    public SIDE side;
    public Vector2 spawnCooldownRange;  // spawn cooldown is random between x and y
    public int initialBoidsCount;
    [Range(2f, 10f)]
    public float spawnRadius;
    public int shipsPerFlock;
}

public class Spawner : MonoBehaviour
{
    public GameObject spaceshipPrefab;
    public GameObject flockPrefab;

    private float nextSpawnTime;
    private SpawnerConfig config;
    private Pooling pooling;
    private ShipManager boidManager;
    private Flock stationingFlock;
    
    private void Update()
    {
        if(stationingFlock && Time.time > nextSpawnTime && boidManager.IsSpawnPossible(config.side))
        {
            if (!stationingFlock.isWaitingForShips)
                SpawnNewStationingFlock();
            
            SpawnNewShip();
            nextSpawnTime = Time.time + Random.Range(config.spawnCooldownRange.x, config.spawnCooldownRange.y);
        }
    }

    public void Initialize(SpawnerConfig config, Pooling pooling, ShipManager boidManager)
    {
        this.pooling = pooling;
        this.config = config;
        this.boidManager = boidManager;

        // register objects in pooling system
        pooling.RegisterObject(spaceshipPrefab, config.initialBoidsCount);
        pooling.RegisterObject(flockPrefab);

        SpawnNewStationingFlock();
        for (int i = 0; i < this.config.initialBoidsCount; i++)
            SpawnNewShip();
    }

    private void SpawnNewStationingFlock()
    {
        stationingFlock = pooling.GetInstance(flockPrefab).GetComponent<Flock>();
        stationingFlock.Initialize(config.shipsPerFlock);
        stationingFlock.transform.position = transform.position;
        stationingFlock.gameObject.SetActive(true);
    }

    private void SpawnNewShip()
    {
        Vector3 positionRand = 
            new Vector3(Random.Range(-config.spawnRadius, config.spawnRadius),
            Random.Range(-config.spawnRadius, config.spawnRadius),
            Random.Range(-config.spawnRadius, config.spawnRadius));
        
        Ship newShip = pooling.GetInstance(spaceshipPrefab).GetComponent<Ship>();
        newShip.transform.position = transform.position + positionRand;
        newShip.Initialize(config.side, boidManager, pooling, positionRand.normalized);
        stationingFlock.AddNewShip(newShip);
        boidManager.AddSpawnedShip(newShip, config.side);
    }
}
