﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Simple pooling system.
 */

using System.Collections.Generic;
using UnityEngine;

public class Pooling : MonoBehaviour
{
    private Dictionary<GameObject, BoostedList<Transform>> objectsInUse;
    private Dictionary<GameObject, BoostedList<Transform>> objectsReady;
    private Transform pooledObjectsContainer; 

    private void Awake()
    {
        objectsInUse = new Dictionary<GameObject, BoostedList<Transform>>();
        objectsReady = new Dictionary<GameObject, BoostedList<Transform>>();
        pooledObjectsContainer = new GameObject("Pooled objects").transform;
    }

    public void RegisterObject(GameObject prefab, int prewarmCount = 0)
    {
        if(!objectsInUse.ContainsKey(prefab))
            objectsInUse.Add(prefab, new BoostedList<Transform>());
        if(!objectsReady.ContainsKey(prefab))
            objectsReady.Add(prefab, new BoostedList<Transform>());

        if (prewarmCount > 0)
            CreateNewInstances(prefab, prewarmCount);
    }
    
    public Transform GetInstance(GameObject prefab)
    {
        if (objectsReady[prefab].Count > 0)
            return GetLastReadyInstance(prefab);
        else
        {
            // find if there are disabled objects that can be re-used
            for (int i = 0; i < objectsInUse[prefab].Count; i++)
                if (!objectsInUse[prefab][i].gameObject.activeSelf)
                {
                    objectsReady[prefab].Add(objectsInUse[prefab][i]);
                    objectsInUse[prefab].Remove(objectsInUse[prefab][i]);
                }

            // if there was nothing found - create new instance
            if (objectsReady[prefab].Count == 0)
                CreateNewInstances(prefab);
            return GetLastReadyInstance(prefab);
        }
    }
    
    private Transform GetLastReadyInstance(GameObject prefab)
    {
        Transform instance = objectsReady[prefab][objectsReady[prefab].Count - 1];
        objectsReady[prefab].Remove(instance);
        objectsInUse[prefab].Add(instance);
        return instance;
    }

    private void CreateNewInstances(GameObject prefab, int count = 1)
    {
        while (count > 0)
        {
            objectsReady[prefab].Add(Instantiate(prefab, pooledObjectsContainer).transform);
            count--;
        }
    }
}
