﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Creating spawners
 */

using System.Collections.Generic;
using UnityEngine;

public class WorldBuilder : MonoBehaviour
{
    public bool createDebugUI;
    public GameObject debugUIPrefab;

    public GameObject spawnerPrefab;
    public SpawnerConfig[] spawnersConfig;

    private List<Spawner> spawnersA;
    private List<Spawner> spawnersB;

    private Pooling pooling;
    private ShipManager boidManager;


    private void Awake()
    {
        spawnersA = new List<Spawner>();
        spawnersB = new List<Spawner>();
        pooling = GetComponent<Pooling>();
        boidManager = GetComponent<ShipManager>();

        if(createDebugUI)
            Instantiate(debugUIPrefab).GetComponent<DebugUI>().boidManager = boidManager;
    }

    private void Start()
    {
        Transform spawnersContainer = new GameObject("Spawners").transform;

        for (int i = 0; i < spawnersConfig.Length; i++)
        {
            Spawner newSpawner = Instantiate(spawnerPrefab, spawnersConfig[i].position, Quaternion.identity, spawnersContainer).GetComponent<Spawner>();
            newSpawner.Initialize(spawnersConfig[i], pooling, boidManager);

            string spawnerID = "Spawner " + spawnersConfig[i].side.ToString() + " ";

            if (spawnersConfig[i].side == SIDE.a)
            {
                spawnersA.Add(newSpawner);
                spawnerID += spawnersA.Count.ToString();
            }
            else
            {
                spawnersB.Add(newSpawner);
                spawnerID += spawnersB.Count.ToString();
            }
            newSpawner.gameObject.name = spawnerID;
        }
    }
}
