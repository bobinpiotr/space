﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Shooting script.
 */

using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject rocketPrefab;
    
    public float attackRange;
    public float reloadDuration;
    public float abandonTargetRange;
    private float readyToFireTime;

    private Ship ship;
    private Rigidbody rigid;
    private Pooling pooling;

    private void Awake()
    {
        ship = GetComponent<Ship>();
        rigid = GetComponent<Rigidbody>();
    }

    public void TryToShoot()
    {
        // target available & weapon ready & target in range
        if (Time.time > readyToFireTime
            && !ReferenceEquals(ship.target, null))
        {
            float distanceFromTarget = Vector3.Distance(transform.position, ship.target.transform.position);
            if (distanceFromTarget <= attackRange)
                Shoot();
            // abandon target if it's too far to prevent ships dispersal
            else if (distanceFromTarget > abandonTargetRange)
                ship.AbandonTarget();
        }
    }

    public void Initialize(Pooling pooling)
    {
        this.pooling = pooling;
        pooling.RegisterObject(rocketPrefab);
    }

    private void Shoot()
    {
        readyToFireTime = Time.time + reloadDuration;

        Rocket rocket = pooling.GetInstance(rocketPrefab).GetComponent<Rocket>();
        rocket.transform.position = transform.position;
        rocket.Launch(ship.target.transform, rigid.velocity, tag);
    }
}
