﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Flock controller script. 
 *  Flock will wait until desired ships count is reached and then head out to the middle of map (Vector.zero).
 */
 
using UnityEngine;

public class Flock : MonoBehaviour
{
    public float flockSpeed;

    private int shipsPerFlock;
    private int currentShipsCount;
    public bool isWaitingForShips { get; private set; }
        
    private void OnEnable()
    {
        isWaitingForShips = true;
    }

    private void Update()
    {
        if (isWaitingForShips && currentShipsCount >= shipsPerFlock)
            isWaitingForShips = false;

        if (!isWaitingForShips)
        {
            if (transform.position != Vector3.zero)
                transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, Time.deltaTime * flockSpeed);
            else
                enabled = false;
        }
    }

    public void Initialize(int shipsPerFlock)
    {
        this.shipsPerFlock = shipsPerFlock;
        isWaitingForShips = true;
        enabled = true;
        currentShipsCount = 0;
    }

    public void AddNewShip(Ship ship)
    {
        ship.GetComponent<Boid>().AssignToNewFlock(this);
        ship.controllingFlock = this;
        currentShipsCount++;
    }

    public void ShipDestroyed()
    {
        if (currentShipsCount <= 1)
            DisableFlock();
        else
            currentShipsCount--;
    }

    private void DisableFlock()
    {
        gameObject.SetActive(false);
    }
}
