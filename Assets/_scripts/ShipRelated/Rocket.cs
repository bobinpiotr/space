﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Rocket controller script.
 */

using UnityEngine;

public class Rocket : MonoBehaviour
{
    public float maxSpeed;
    // Rotation speed per second in radians
    public float rotationSpeed;
    public float acceleration;
    // How much time rocket have to reach its target
    public float lifeTime;
    private float autoDestructionTime;

    private Transform target;
    private Rigidbody rigid;
    private SphereCollider sphereCollider;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        sphereCollider = GetComponent<SphereCollider>();
    }

    private void Update()
    {
        if (!target)
            return;

        if (Time.time > autoDestructionTime)
            AutoDestruction();
        else
        {
            Vector3 targetVelocity = rigid.velocity;
            targetVelocity += (target.position - transform.position).normalized * maxSpeed;

            targetVelocity = Vector3.ClampMagnitude(targetVelocity, maxSpeed);
            Vector3 newVelocity = Vector3.RotateTowards(rigid.velocity, targetVelocity, rotationSpeed * Time.deltaTime, acceleration * Time.deltaTime);
            rigid.velocity = newVelocity;
            
            transform.rotation = Quaternion.LookRotation(newVelocity);
        }
    }

    // Destruction upon collision with enemy ship
    private void OnCollisionEnter(Collision collision)
    {
        Ship hitShip = collision.gameObject.GetComponent<Ship>();
        if(hitShip && !CompareTag(hitShip.tag))
        {
            SetRocketState(false);
            hitShip.ReceiveDamage(1);
        }
    }

    public void Launch(Transform target, Vector3 initialVelocity, string shooterTag)
    {
        tag = shooterTag;
        autoDestructionTime = Time.time + lifeTime;

        this.target = target;
        rigid.velocity = initialVelocity;
        SetRocketState(true);
    }
    
    // Disable / enable rocket
    private void SetRocketState(bool state)
    {
        sphereCollider.enabled = state;
        gameObject.SetActive(state);
    }

    private void AutoDestruction()
    {
        SetRocketState(false);
    }
}
