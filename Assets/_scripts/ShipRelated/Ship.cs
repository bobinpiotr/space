﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Ship controller script.
 *  Manages: target, life / death cycle, materials (color).
 */

using UnityEngine;

public enum SIDE
{
    a,
    b,
}

public class Ship : MonoBehaviour
{
    public Material materialA;
    public Material materialB;
    public Material materialDestroyed;

    // Notify chasing ships that current target is destroyed
    public delegate void OnDestroyShip();
    public OnDestroyShip onDestroyShip;
    
    // Current target
    public Ship target { get; private set; }

    // Health
    public int maxHealth = 3;
    private int currentHealth;
    public bool isAlive { get { return currentHealth > 0; } }

    public SIDE side { get; private set; }

    // How long ships will be falling down before disabling
    public float timeToDisable = 5f;

    // flock to which this ship is assigned to
    public Flock controllingFlock { get; set; }

    private Boid boid;
    private ShipManager boidManager;
    private Shooting shooting;
    private Rigidbody rigid;
    private TrailRenderer trailRenderer;
    private MeshRenderer[] meshes;

    private void Awake()
    {
        boid = GetComponent<Boid>();
        rigid = GetComponent<Rigidbody>();
        shooting = GetComponent<Shooting>();
        trailRenderer = GetComponentInChildren<TrailRenderer>();
        meshes = GetComponentsInChildren<MeshRenderer>();
    }
    
    public void ManualUpdateShip()
    {
        boid.UpdateTargetVelocity();
        shooting.TryToShoot();
        if (ReferenceEquals(target, null) || !target.isAlive)
            LookForNewTarget();
    }
    
    public void UpdateCurrentVelocity(int framesPerFullUpdate)
    {
        if(isAlive)
            boid.UpdateCurrentVelocity(framesPerFullUpdate);
    }

    // Try to find new target from inside collider
    public void LookForNewTarget()
    {
        Ship newTarget = boid.GetRandomEnemy();

        if (!ReferenceEquals(newTarget, null))
            SetNewTarget(newTarget);
        else
            target = null;
    }
    
    public void SetNewTarget(Ship target)
    {
        this.target = target;
        // when target is destroyed every chasing unit will look for new target
        target.onDestroyShip += LookForNewTarget;
    }

    public void AbandonTarget()
    {
        target.onDestroyShip -= LookForNewTarget;
        target = null;
    }

    public void Initialize(SIDE side, ShipManager boidManager, Pooling pooling, Vector3 initialVelocity)
    {
        enabled = true;
        this.boidManager = boidManager;
        this.side = side;
        currentHealth = maxHealth;

        rigid.useGravity = false;
        rigid.velocity = initialVelocity;

        GetComponent<Shooting>().Initialize(pooling);

        Material selectedMaterial = null;
        switch (side)
        {
            case SIDE.a:
                tag = "A";
                selectedMaterial = materialA;
                break;

            case SIDE.b:
                tag = "B";
                selectedMaterial = materialB;
                break;
        }

        SetShipMaterials(selectedMaterial);
        gameObject.SetActive(true);
    }

    private void SetShipMaterials(Material material)
    {
        foreach (MeshRenderer meshRenderer in meshes)
            meshRenderer.material = material;
        trailRenderer.material = material;
    }

    public void ReceiveDamage(int amount)
    {
        if (currentHealth > 0)
        {
            currentHealth -= amount;
            if (currentHealth <= 0)
                Death();
        }
    }

    public void Death()
    {
        enabled = false;
        rigid.useGravity = true;
        controllingFlock.ShipDestroyed();

        // destroyed ship no longer need to update its target
        if(!ReferenceEquals(target, null))  
            target.onDestroyShip -= LookForNewTarget;
        target = null;

        if (onDestroyShip != null)  
        {
            onDestroyShip();
            onDestroyShip = null;
        }
        SetShipMaterials(materialDestroyed);
        boid.ClearNeighboursLists();
        boidManager.RemoveDestroyedShip(this, side);

        Invoke("DisableShip", timeToDisable);
    }

    private void DisableShip()
    {
        gameObject.SetActive(false);
    }
}
