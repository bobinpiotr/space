﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  Movement controller script, based on boid / flocking concept.
 */
 
using UnityEngine;

public class Boid : MonoBehaviour
{
    [Header("Movement config")]
    public float acceleration;
    public float minSpeed;
    public float maxSpeed;
    [Tooltip("Rotation speed per second in radians.")]
    public float rotationSpeed;

    /// Attraction force - fly toward target position; gets stronger with increased distance
    [Header("Attraction force")]
    public float attractionPower;
    public float attractionRange;

    /// Separation force - avoid closest boids
    [Header("Separation force")]
    public float separationPower;
    public float separationRange;

    // List of allied ships inside collider.
    private BoostedList<Transform> alliedShips;
    // List of enemy ships inside collider.
    private BoostedList<Ship> enemyShips;
    // Current velocity will be rotated towards targetVelocity
    private Vector3 targetVelocity;

    private Ship ship;
    private Rigidbody rigid;
    private Flock flock;

    private void Awake()
    {
        ship = GetComponent<Ship>();
        rigid = GetComponent<Rigidbody>();
        alliedShips = new BoostedList<Transform>();
        enemyShips = new BoostedList<Ship>();
    }

    // add allies and enemies in range
    private void OnTriggerEnter(Collider other)
    {
        // ignore if it's object from other layer(like rocket) or this ship is destroyed
        if (other.gameObject.layer != gameObject.layer || !ship.isAlive)
            return;

        Ship otherShip = other.GetComponent<Ship>();

        // if object have ship component and is alive - add it to proper list
        if (!ReferenceEquals(otherShip, null) && otherShip.isAlive)
        {
            if (otherShip.side == ship.side && !alliedShips.Contains(other.transform))
                alliedShips.Add(other.transform);
            else if (otherShip.side != ship.side && !enemyShips.Contains(otherShip))
            {
                enemyShips.Add(otherShip);

                // Set ship as target if there wasn't any enemies in collider range before
                if (enemyShips.Count == 1)
                    ship.SetNewTarget(otherShip);
            }
        }
    }

    // remove allies and enemies out of range
    private void OnTriggerExit(Collider other)
    {
        // ignore if it's object from other layer(like rocket) or this ship is destroyed
        if (other.gameObject.layer != gameObject.layer || !ship.isAlive)
            return;

        Ship otherShip = other.GetComponent<Ship>();

        if (!ReferenceEquals(otherShip, null))
        {
            if (otherShip.side == ship.side && alliedShips.Contains(other.transform))
                alliedShips.Remove(other.transform);
            else if (otherShip.side != ship.side && enemyShips.Contains(otherShip))
                enemyShips.Remove(otherShip);
        }
    }

    public void UpdateCurrentVelocity(int framesPerFullUpdate)
    {
        // Rotation speed and acceleration are multiplied by framesPerFullUpdate to ensure that rotation and acceleration are independent from update rate.
        Vector3 newVelocity = Vector3.RotateTowards(rigid.velocity, targetVelocity,
            rotationSpeed * framesPerFullUpdate * Time.deltaTime,
            acceleration * framesPerFullUpdate * Time.deltaTime);
        rigid.velocity = newVelocity;

        transform.rotation = Quaternion.LookRotation(newVelocity);
    }

    public void UpdateTargetVelocity()
    {
        targetVelocity = rigid.velocity;
  
        targetVelocity += CalculateSeparationForce();

        if (ReferenceEquals(ship.target, null))
            targetVelocity += CalculateAttractionForce(flock.transform);
        else
            targetVelocity += CalculateAttractionForce(ship.target.transform);

        // clamp target velocity speed
        float magnitude = targetVelocity.magnitude;
        if (magnitude < minSpeed)
            targetVelocity = targetVelocity.normalized * minSpeed;
        else if (magnitude > maxSpeed)
            targetVelocity = targetVelocity.normalized * maxSpeed;
    }

    // Calculate force required to steer towards target position.
    private Vector3 CalculateAttractionForce(Transform target)
    {
        float distanceFromTarget = Vector3.Distance(transform.position, target.position);
        Vector3 attractionForce = (target.position - transform.position) * attractionPower;

        if (distanceFromTarget < attractionRange)
            attractionForce *= (attractionRange - distanceFromTarget) / attractionRange;

        return attractionForce;
    }

    // Calculate force required to avoid collisions with other ships.
    private Vector3 CalculateSeparationForce()
    {
        Vector3 separationForce = Vector3.zero;
        Vector3 boidPosition = transform.position;
        for (int i = 0; i < alliedShips.Count; i++)
        {
            Vector3 processedBoidPosition = alliedShips[i].position;
            float distanceFromNeighbour = Vector3.Distance(boidPosition, processedBoidPosition);
            if (distanceFromNeighbour < separationRange)
            {
                // separationFromNeighbour is inversely proportional to distance from the neighbour.
                Vector3 separationFromNeighbour = (separationRange - distanceFromNeighbour) / separationRange
                    * separationPower
                    * (boidPosition - processedBoidPosition);
                separationForce += separationFromNeighbour;
            }
        }

        return separationForce;
    }
    
    public void AssignToNewFlock(Flock flock)
    {
        this.flock = flock;
        // Set initial target velocity value
        UpdateTargetVelocity();
    }

    // Return random enemy if there is any in range.
    public Ship GetRandomEnemy()
    {
        while(enemyShips.Count > 0)
        {
            // return active enemy
            if (enemyShips[enemyShips.Count - 1].isAlive)
                return enemyShips[enemyShips.Count - 1];
            // remove inactive enemy and keep looking
            else
                enemyShips.Remove(enemyShips[enemyShips.Count - 1]);
        }
        return null;
    }
    
    public void ClearNeighboursLists()
    {
        alliedShips.Clear();
        enemyShips.Clear();
    }
}