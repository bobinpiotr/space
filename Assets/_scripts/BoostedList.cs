﻿/*  
 *  Piotr Bobin, 2017, interview project
 *  List with dictionary to improve remove time - only 1 element is moved when something is removed.
 */

using System.Collections.Generic;

public class BoostedList<T>
{
    private List<T> list;
    private Dictionary<T, int> indexDictionary;

    public int Count { get { return list.Count; } }

    public BoostedList()
    {
        list = new List<T>();
        indexDictionary = new Dictionary<T, int>();
    }

    public void Add(T element)
    {
        indexDictionary.Add(element, list.Count);
        list.Add(element);
    }

    public void Remove(T element)
    {
        int elementIndex = indexDictionary[element];

        // If selected ship is not last and there is more than 1 element in list, move last element in place of selected to remove
        if (elementIndex != list.Count - 1 && list.Count > 1)
        {
            list[elementIndex] = list[list.Count - 1];

            // Update moved element index
            indexDictionary[list[elementIndex]] = elementIndex;
        }

        // remove last element from list and dictionary entry
        list.RemoveAt(list.Count - 1);
        indexDictionary.Remove(element);
    }

    public bool Contains(T element)
    {
        return indexDictionary.ContainsKey(element);
    }

    public void Clear()
    {
        list.Clear();
        indexDictionary.Clear();
    }
    
    public T this [int index]
    {
        get { return list[index]; }
    }
}
